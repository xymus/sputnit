INKSCAPE_DIR ?= $(shell nitls -pP inkscape_tools)

all: bin/sputnit bin/sputnit.apk

bin/sputnit: $(shell nitls -M src/sputnit_linux.nit src/debug.nit) gen
	nitc -o $@ src/sputnit_linux.nit -m src/debug.nit -m src/gen/serial_linux.nit

bin/sputnit.apk: $(shell nitls -M src/sputnit_android.nit) gen android/res
	nitc -o $@ src/sputnit_android.nit

android-release: $(shell nitls -M src/sputnit_android.nit) gen android/res
	nitc -o $@ src/sputnit_android.nit --release

android/res: art/icon.svg
	make -C ${INKSCAPE_DIR}
	${INKSCAPE_DIR}/bin/svg_to_icons --out android/res --android art/icon.svg

gen: src/gen/characters_spritesheet.nit src/gen/serial_linux.nit

src/gen/serial_linux.nit: $(shell nitls -M src/sputnit_linux.nit)
	nitserial -o $@ src/sputnit_linux.nit

src/gen/characters_spritesheet.nit: art/characters_spritesheet.svg
	make -C ${INKSCAPE_DIR}
	${INKSCAPE_DIR}/bin/svg_to_png_and_nit art/characters_spritesheet.svg -a assets/ -s src/gen/ -x 2.0 -g

# ---
# Quick compile and launch

run: bin/sputnit
	bin/sputnit --cheat-all-techs

run-android: bin/sputnit.apk
	adb install -rd bin/sputnit.apk
	adb logcat | grep app.nit

# ---
# Tests

check:
	nitunit src/tests src/game
