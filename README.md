
# Key bindings

`wasd` & arrow keys
:	Move the camera.

scroll wheel
:	Zoom in and out.

`,` & `.`
:	Slow down and speed up the game.

spacebar
:	Pause and resume the game.

`k` & `l`
:	Save and load the game.

# Artwork

Some artwork created by Sylwia Bartyzel, Arthur Shlain, kenney.nl and hackcraft.de.

Sounds created by kenney.nl, published under CC0.
