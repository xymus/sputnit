module test_path is test_suite
import test_suite
import path
import story

class TestPath
	super TestSuite

	var g = new Game
	var station: Station = g.station

	init do
		var lobby = g.story.lobby
		var stair = g.story.stair

		var l1 = lobby.try_spawn(station.lot(0,0)).spawn
		var l2 = lobby.try_spawn(station.lot(0,1)).spawn
		var l3 = lobby.try_spawn(station.lot(2,0)).spawn
		var s1 = stair.try_spawn(station.lot(-1,0)).spawn
		var s2 = stair.try_spawn(station.lot(-1,1)).spawn
		var l4 = lobby.try_spawn(station.lot(0,2)).spawn
	end

	fun test_graph
	do
		station.dump

		for r in station.rooms do for s in r.lots do
			print "{s}"
			for w in s.walk do
				print "\t{w})"
			end
		end
	end

	fun test_path
	do

		var p = station.lot(3,0).path_to(station.lot(1,1))
		if p != null then print p.plan else print "-"

		p = station.lot(3,0).path_to(station.lot(1,2))
		if p != null then print p.plan else print "-"
	end
end
