module test_game is test_suite
import test_suite
import game
import story
import debug

class TestGame
	super TestSuite

	fun test_game
	do
		var g = new Game

		g.setup

		for i in [0..11[ do g.update(0.5)
		g.story.lobby.try_spawn(g.station.lot(2,0)).spawn
		g.story.lobby.try_spawn(g.station.lot(4,0)).spawn
		g.story.stair.try_spawn(g.station.lot(6,0)).spawn
		g.story.stair.try_spawn(g.station.lot(6,1)).spawn
		g.story.lobby.try_spawn(g.station.lot(4,1)).spawn
		g.story.lobby.try_spawn(g.station.lot(2,1)).spawn
		g.story.lobby.try_spawn(g.station.lot(0,1)).spawn
		for i in [0..22[ do g.update(0.5)

		g.station.dump
	end
end
