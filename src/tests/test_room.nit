module test_room is test_suite
import test_suite
import room

redef class Story
	redef var empty_room = new EmptyRoom(self)
	redef var lobby      = new RoomRule(self,  "Lobby",        2,1,  daily_cost= 10)
end

class TestRoom
	super TestSuite

	fun test_lots
	do
		var g = new Game
		var station = g.station

		for i in [-10..10] do for j in [-10..10] do
			var lot = station.lot(i,j)
			assert lot.x == i
			assert lot.y == j
		end
	end

	fun test_room
	do
		var g = new Game
		var station = g.station

		var lobby = g.story.lobby

		for i in [-10..10] do for j in [-10..10] do
			var lot = station.lot(i,j)
			assert lot.room == null
			assert not lobby.try_spawn(lot).is_ok
		end

		var l1 = lobby.try_spawn(station.lot(0,0)).spawn
		var l2 = lobby.try_spawn(station.lot(0,1)).spawn
		var l3 = lobby.try_spawn(station.lot(2,0)).spawn
		assert not lobby.try_spawn(station.lot(0,0)).is_ok
		assert not lobby.try_spawn(station.lot(1,1)).is_ok

		l2.destroy
		assert lobby.try_spawn(station.lot(1,1)).is_ok
	end
end
