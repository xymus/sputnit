module story is serialize

import game

redef class Story

	# Rooms                                     name           size
	redef var empty_room = new EmptyRoom(self, "Empty",              cost=20,
		walkable=false)
	redef var lobby      = new RoomRule(self,  "Lobby",        2,1,  daily_cost= 10,           hspeed=1.0)
	redef var terminal   = new TerminalRule(self, "Terminal",  3,1,  daily_cost=100,           hspeed=1.0,
		power=20.0, oxygen=50.0, static=true,
		period_out=0.2, period_in=0.05)
	redef var cable      = new RoomRule(self,  "Cable",        1,40,
		structural=false,        static=true)

	#                                        name        cost
	var stair      = new Stair(self,        "Staircase", cost= 50, daily_cost= 10, vspeed=0.4, hspeed=1.0)
	var escalator  = new Stair(self,        "Escalator", cost=250, daily_cost= 25, vspeed=0.8, hspeed=1.0,
		power=-0.5)
	var airlock    = new Airlock(self,
		            airtight=true)

	var elevator   = new ElevatorRule(self, "Elevator",  cost=200,                             hspeed=1.0,
		max_cars=8, car_capacity=8,
		power=-2.0, airtight=true)

	#                                              name           size cost
	var research_lab = new RentableRoomRule(self, "Research lab", 4,1, 200, roles=[work]*4,             daily_rent=  20,
		power_per_roles=-1.0)
	var bar          = new RentableRoomRule(self, "Bar",          4,1, 400, roles=[work]*2+[leisure]*8, daily_rent=  20,
		power=-2.0)
	var sleep_pod    = new RentableRoomRule(self, "Sleep pod",    1,1,  50, roles=[residence]*1,        daily_rent=   5)
	var condominium  = new BuyableRoomRule(self,  "Condominium",  3,1, 100, roles=[residence]*2,     one_time_cost= 200)
	var dock         = new BuyablePortRule(self,  "Dock",         5,3, 500, roles=[work]*6,          one_time_cost=1000,
		power=-10.0,
		period_out=0.5, period_in=0.1)

	var solar        = new RoomRule(self,         "Solar",        4,1, 200, structural=false,
		power=10.0)
	var reactor      = new RoomRule(self,         "Reactor",      4,3,5000, roles=[work]*4,
		power_per_roles=50.0)

	var o2_generator = new RoomRule(self,         "O2 Generator", 4,1, 250, roles=[work]*4,
		power_per_roles=10.0, oxygen_per_roles=10.0)
	var greenhouse = new RoomRule(self,           "Greenhouse",   5,2, 400, roles=[work]*2,
		                      oxygen_per_roles=10.0, structural=false, walkable=true)

	var laser_defense = new DefenseRoomRule(self, "Laser Defense", 2,2, 1000,
		defense_radius=16.0, structural=false)

	# Techs
	var start     = new Tech(self, "Start",           new Array[TechGoal])
	var bootstrap = new Tech(self, "Basic crew",      [new PopGoal( 25)],
		unlocks=[condominium, dock, greenhouse])
	var spaceport = new Tech(self, "Spaceport",       [new PopGoal(100), new RoomGoal(dock, 1): TechGoal],
		unlocks=[escalator, o2_generator, laser_defense])
	var resources = new Tech(self, "Space resources", [new PopGoal(200)],
		unlocks=[reactor])
	var hub       = new Tech(self, "Trade hub",       [new PopGoal(500)])
end

class BuyablePortRule
	super BuyableRoomRule
	super PortRule

	autoinit story=, name=, width=, height=, cost=, refund=, daily_cost=, power=, power_per_roles=, oxygen=, oxygen_per_roles=, airtight=, static=, structural=, walkable=, roles=, period_out=, period_in=, one_time_cost=, max_influx=, hspeed=

	redef fun new_room(station) do return new BuyablePort(self, station)
end

class BuyablePort
	super BuyableRoom
	super Port

	redef type RULE: BuyablePortRule

	redef fun buy(turn)
	do
		super
		# TODO reset on buy and deactivated before
	end
end
