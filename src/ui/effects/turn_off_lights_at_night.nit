import station_view

redef class Game
	# Beginning of the daytime, within `[0.0..1.0[`
	var dawn: Float = 0.2

	# End of the daytime, within `[0.0..1.0[`
	var dusk: Float = 0.9

	# Is it currently daytime?
	fun daytime: Bool do return time_of_day > dawn and time_of_day < dusk

	# It is currently at night?
	fun night: Bool do return time_of_day <= dawn or time_of_day >= dusk

	# Proportion of rooms with their lights on
	private fun prop_of_rooms_with_lights_on: Float
	do
		var transition = 0.1
		var p = if daytime then
			((time_of_day - dawn)/transition).clamp(0.0, 1.0)
		else if time_of_day < 0.5 then
			0.0
		else 1.0 - ((time_of_day - dusk)/transition).clamp(0.0, 1.0)
		return p.pow(2.0)
	end
end

redef class Station
	# Rooms with their lights on
	private var rooms_on = new Array[Room]

	# Rooms with their lights off
	private var rooms_off = new Array[Room]

	redef fun update(turn)
	do
		super

		var prop_on = turn.game.prop_of_rooms_with_lights_on
		var t = (rooms.length.to_f * prop_on).to_i
		#print "{prop_on} {t}/{rooms.length}"
		if prop_on > 0.0 and rooms_off.not_empty then
			while rooms_on.length < t and rooms_off.not_empty do
				var r = rooms_off.rand
				if r.rule.darken_at_night then
					for l in r.lots do
						var s = l.sprite
						if s != null then s.darken false
					end
					for s in r.sprites do s.darken false
				end
				rooms_off.remove r
				rooms_on.add r
			end
		end

		if prop_on < 1.0 and rooms_on.not_empty then
			while rooms_on.length > t do
				var r = rooms_on.rand
				if r.rule.darken_at_night then
					for l in r.lots do
						var s = l.sprite
						if s != null then s.darken true
					end
					for s in r.sprites do s.darken true
				end
				rooms_on.remove r
				rooms_off.add r
			end
		end
	end
end

redef class Room
	redef fun on_spawn
	do
		super
		station.rooms_on.add self
	end

	redef fun on_destroy
	do
		super
		if station.rooms_on.has(self) then station.rooms_on.remove self
		if station.rooms_off.has(self) then station.rooms_off.remove self
	end

	redef fun display_furniture
	do
		super
		if not station.rooms_on.has(self) and rule.darken_at_night then
			for s in sprites do s.darken true
		end
	end
end

redef class Sprite
	private fun darken(dark: Bool)
	do
		var mod = if dark then 0.5 else 1.0/0.5
		for i in 3.times do tint[i] = tint[i]*mod
	end
end

redef class RoomRule
	# Should this room turn its lights off at night?
	fun darken_at_night: Bool do return structural and (self isa BuyableRoomRule or self isa RentableRoomRule)
end
