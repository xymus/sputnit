import station_view

redef class Station

	private var sprites_unreachable_room = new Array[Sprite]

	private fun update_reachable_room
	do
		app.sprites.remove_each sprites_unreachable_room

		for r in rooms do
			if r.rule.walkable and not r.reachable_from_terminal then
				var s = new Sprite(app.texture_warning,
					new Point3d[Float](r.screen_x, r.screen_y, 1.0))
				s.tint[1] = 0.0
				s.tint[2] = 0.0
				s.scale = 0.5

				app.sprites.add s
				sprites_unreachable_room.add s
			end
		end
	end
end

redef class Room
	redef fun on_spawn
	do
		super
		station.update_reachable_room
	end

	redef fun destroy
	do
		super
		station.update_reachable_room
	end
end

redef class Elevator
	redef fun on_join_lot(lot)
	do
		super
		station.update_reachable_room
	end
end
