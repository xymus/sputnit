import station_view

import gamnit

# TODO optimize walls, don't overlap and use a signle floor per continuous deck
# TODO add doors to internal walls
# TODO fix walls for elevator on reloading

redef class Room
	var walls = new Array[Actor]

	redef fun display
	do
		super

		if rule.structural and rule != rule.story.empty_room then
			# Add floor
			add_walls_around(lots)
		end
	end

	# Add floor and walls around `lots`
	fun add_walls_around(lots: Array[Lot])
	do
		var left = new Point[Float](lots.first.screen_x-lot_width/2.0, lots.first.screen_y-lot_height/2.0)
		var right = new Point[Float](left.x+rule.width.to_f*lot_width, left.y)
		var ceiling_left = new Point[Float](left.x, left.y+rule.height.to_f*lot_height)
		var ceiling_right = new Point[Float](right.x, right.y+rule.height.to_f*lot_height)

		var wall_mesh = new MeshWalls([left, right, ceiling_right, ceiling_left])
		var wall_material = new SmoothMaterial([0.25,0.25,0.25,1.0], [0.0]*4, [0.0]*4)
		var wall_model = new LeafModel(wall_mesh, wall_material)
		var wall = new Actor(wall_model,
			new Point3d[Float](0.0, 0.0, 0.0))

		app.actors.add wall
		walls.add wall
	end

	redef fun on_destroy
	do
		super

		# Remove walls
		var walls = walls
		if walls.not_empty then
			app.actors.remove_each walls
			self.walls.clear
		end
	end
end

redef class Elevator
	redef fun on_join_lot(lot)
	do
		super
		add_walls_around([lot])
	end
end

# Simple walls/floor defined by 2D points on X/Y, covers Z from 0.0 to 20.0
class MeshWalls
	super Mesh

	var points: Array[Point[Float]]
	var z = 20.0

	redef var vertices is lazy do
		var vertices = new Array[Float]
		for point in points do
			vertices.add point.x
			vertices.add point.y
			vertices.add 0.0
			vertices.add point.x
			vertices.add point.y
			vertices.add z
		end
		return vertices
	end

	redef var texture_coords: Array[Float] is lazy do
		var vertices = new Array[Float]
		for point in points do
			for i in 2.times do vertices.add_all([0.0, 0.0]) # TODO
		end

		return vertices
	end

	# FIXME if used
	redef var center = new Point3d[Float](0.0, 0.0, 0.0) is lazy

	init
	do
		for i in points.length.times do
			var j = (i+1)%points.length
			indices.add i*2
			indices.add i*2+1
			indices.add j*2
			indices.add j*2
			indices.add j*2+1
			indices.add i*2+1
		end
	end
end
