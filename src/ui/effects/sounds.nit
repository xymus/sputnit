import station_view

redef class App
	var cash_out_sounds: Array[Sound] = [
		new Sound("sounds/chipsHandle1.wav"),
		new Sound("sounds/chipsHandle3.wav")]

	var cash_in_sounds: Array[Sound] = [
		new Sound("sounds/chipsStack1.wav"),
		new Sound("sounds/chipsStack3.wav")]

	var tech_level_up_sound = new Sound("sounds/objective_achieved.mp3")

	var click_sound = new SoundAndVibration("sounds/click3.mp3", 40)

	var lot_over_sound = new SoundAndVibration("sounds/click4.mp3", 20)

	var sounds_in_frame = 0

	var last_frame_with_sound = 0.0

	# Play one of `sounds` at random and not more than a few per frames
	private fun play_random_sound(sounds: Array[Sound])
	do
		var time = game.time
		if time != last_frame_with_sound then
			sounds_in_frame = 1
			last_frame_with_sound = time
		else if sounds_in_frame < 4 then
			sounds_in_frame += 1
		else return

		sounds.rand.play
	end

	redef fun last_over_lot=(value)
	do
		if last_over_lot != value then lot_over_sound.play
		super
	end
end

class SoundAndVibration
	super Sound

	var vibration_ms: Int
end

redef class Cash
	redef fun inc(u, from, reason)
	do
		if u > 0 then app.play_random_sound app.cash_in_sounds
		return super
	end

	redef fun dec(u, from, reason)
	do
		if u > 0 then app.play_random_sound app.cash_out_sounds
		return super
	end
end

redef class Station
	redef fun on_tech_level_up(turn)
	do
		super
		if tech_level.order != 0 and not cheat_all_techs then app.tech_level_up_sound.play
	end
end

redef class Button
	redef fun on_click
	do
		app.click_sound.play
		super
	end
end

redef class DefenseRoom
	redef fun open_fire(x, y)
	do
		if rule.open_fire_sounds.is_empty then return
		rule.open_fire_sounds.rand.play
	end
end

