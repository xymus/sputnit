import gamnit::flat
import app::audio

import story

redef class Story
	fun load_assets
	do
		for rule in rules do rule.load_assets

		bar.furniture.add new Texture("textures/furniture/bar-table.png")
		bar.furniture_per_room = 6

		research_lab.furniture.add new Texture("textures/furniture/lab-table.png")

		sleep_pod.furniture.add new Texture("textures/furniture/sleeppod-pod.png")

		condominium.furniture.add new Texture("textures/furniture/condo-bed.png")
		condominium.furniture.add new Texture("textures/furniture/condo-plant.png")
		condominium.furniture.add new Texture("textures/furniture/condo-table.png")

		lobby.furniture.add condominium.furniture[1]
		lobby.furniture_per_room = 1

		greenhouse.furniture.add condominium.furniture[1]
		greenhouse.furniture_per_room = 8
		greenhouse.wide_texture = new Texture("textures/room/greenhouse_wide.png")

		dock.furniture.add new Texture("textures/furniture/dock-boxes.png")
		reactor.furniture.add new Texture("textures/furniture/reactor-box.png")
		o2_generator.furniture.add new Texture("textures/furniture/o2generator-box.png")

		laser_defense.wide_texture = new Texture("textures/room/laserdefense_wide.png")
		laser_defense.open_fire_sounds.add new Sound("sounds/laser4.mp3")
		laser_defense.open_fire_sounds.add new Sound("sounds/laser5.mp3")
		laser_defense.open_fire_sounds.add new Sound("sounds/laser6.mp3")
	end
end

redef class Rule
	fun load_assets do end

	private fun mangled_name: String
	do return name.to_s.replace(" ", "").to_lower
end

redef class RoomRule
	# Background texture for each lot
	var texture = new Texture("textures/room/{mangled_name}.png") is lazy

	# Single texture covering the full back of the room, replaces per lot texture
	var wide_texture: nullable Texture = null

	# Furniture texture added randomly to the room
	var furniture = new Array[Texture]

	# Number of `furniture` items to add to each room
	var furniture_per_room: Int = width is lazy

	redef fun load_assets do texture
end

redef class PortRule
	var ship_texture = new Texture("textures/{mangled_name}_ship.png") is lazy

	redef fun wide_texture do return wide_texture_closed

	var wide_texture_closed = new Texture("textures/room/{mangled_name}_closed.png") is lazy

	var wide_texture_opened = new Texture("textures/room/{mangled_name}_opened.png") is lazy

	redef fun load_assets
	do
		super
		wide_texture_closed
		wide_texture_opened
		ship_texture
	end
end

redef class DefenseRoomRule
	var open_fire_sounds = new Array[Sound]
end
