import app
import serialization

redef class App
	# Configuration options
	var config = new ClientConfig is lazy
end


# Configuration of the client
class ClientConfig
	serialize

	# Should the client play sounds?
	var play_sounds = true is lazy

	# Allow building new room only if reachable?
	var build_reachable_only = true is lazy
end
