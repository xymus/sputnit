intrude import gamnit::flat_core
intrude import gamnit::depth

redef class App
	# Sprite being drawn at the back of the screen
	#
	# Not affected by `world_camera` position.
	var background_sprites: Set[Sprite] = new SpriteSet

	# Draw the sprites as 3 layers, clearing the depth buffer after each one:
	# 1. `background_sprites`
	# 2. world `sprites`
	# 3. `ui_sprites`
	redef fun frame_core_draw(display)
	do
		glViewport(0, 0, display.width, display.height)
		frame_core_dynamic_resolution_before display

		# Extra
		frame_core_background_sprites display
		perfs["gamnit depth background_sprites"].add perf_clock_main.lapse
		# Extra

		frame_core_depth_clock.lapse
		for actor in actors do
			for leaf in actor.model.leaves do
				leaf.material.draw(actor, leaf, app.world_camera)
			end
		end
		perfs["gamnit depth actors"].add frame_core_depth_clock.lapse

		frame_core_world_sprites display
		perfs["gamnit depth sprites"].add frame_core_depth_clock.lapse

		# Toggle writing to the depth buffer for particles effects
		glDepthMask false
		for system in particle_systems do system.draw
		glDepthMask true
		perfs["gamnit depth particles"].add frame_core_depth_clock.lapse

		frame_core_ui_sprites display
		perfs["gamnit depth ui_sprites"].add frame_core_depth_clock.lapse

		frame_core_dynamic_resolution_after display
	end

	fun frame_core_background_sprites(display: GamnitDisplay)
	do
		# Reset only the depth buffer
		var simple_2d_program = app.simple_2d_program
		simple_2d_program.use
		simple_2d_program.mvp.uniform world_camera.mvp_matrix_no_translation

		# draw
		background_sprites.as(SpriteSet).draw

		glClear gl_DEPTH_BUFFER_BIT
	end
end

redef class EulerCamera

	# Projection and rotation matrix but without translation
	protected fun mvp_matrix_no_translation: Matrix
	do
		var view = rotation_matrix
		var projection = new Matrix.perspective(pi*field_of_view_y/2.0,
			display.aspect_ratio, near, far)

		return view * projection
	end
end
