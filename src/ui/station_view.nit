import gamnit::depth
intrude import gamnit::tileset
import binary::serialization

import game
import lib

import client_config
import characters_spritesheet
import assets

fun lot_width: Float do return 64.0
fun lot_height: Float do return 92.0

redef class App

	var game = new Game
	var save_path = "save.sputnit"

	var buttons = new Array[Button]

	# TODO refactor into selected action or something
	var selected_room_rule: nullable RoomRule = null
	var selected_free = false
	var last_over_lot: nullable Lot = null

	private var texture_bulldozer = new Texture("textures/bulldozer.png")
	var texture_warning = new Texture("textures/warning.png")
	var texture_checkmark = new Texture("textures/checkmark.png")
	var texture_cross = new Texture("textures/cross.png")
	var texture_star = new Texture("textures/star.png")
	var texture_bolt = new Texture("textures/bolt.png")
	var texture_o2 = new Texture("textures/O2.png")

	private var texture_elevator_car = new Texture("textures/elevator_car.png")

	var texture_for_sale = new Texture("textures/for-sale.png")
	var texture_for_rent = new Texture("textures/for-rent.png")

	# Sprites half-transparent shown when overing
	var over_sprites = new Array[Sprite]

	var characters_spritesheet = new Characters_SpritesheetImages

	var texture_font = new Texture("fonts/berlin.png")
	var font = new TileSetFont(texture_font, 32, 32, " "*32 + """
 !"#$%&'()*+,-./
0123456789:;<=>?
@ABCDEFGHIJKLMNO
PQRSTUVWXYZ[\\]^_
`abcdefghijklmno
pqrstuvwxyz{|}~ """ + " "*(8*16))


	var sprites_status = new TextSprites(font, ui_camera.top_left.offset(16, -16, 0)) is lazy

	# General info display
	var sprites_describe = new TextSprites(font, ui_camera.top_right.offset(-16, -16, 0)) is lazy

	var sprites_goals = new TextSprites(font, ui_camera.top_left.offset(24.0, -92, 0)) is lazy

	var tech_level_stars = new Array[Sprite]
	var power_rating_bolts = new Array[Sprite]

	var icon_o2 = new Sprite(texture_o2, ui_camera.top_left.offset(364, -64, 0)) is lazy
	var sprites_o2 = new TextSprites(font, ui_camera.top_left.offset(400, -56, 0)) is lazy

	# Is the game on pause?
	var paused = false

	# Game speed
	var game_speed = 1.0

	fun mod_game_speed(mod: Float)
	do
		if mod > 1.0 and paused then
			paused = false
			game_speed = 1.0
		else if mod < 1.0 and game_speed == 1.0 then
			paused = true
		else
			game_speed *= mod
			game_speed = game_speed.clamp(1.0, 64.0)
		end
	end

	redef fun on_create
	do
		# Force register room textures
		game.story.load_assets

		super

		# TODO splash screen

		for model in models do model.load
		font.hspace = -16
		font.vspace = -4

		# Insert extra glyphs
		font.chars = font.chars_cleaned.to_s + "✓✘★"
		font.subtextures.add texture_checkmark
		font.subtextures.add texture_cross
		font.subtextures.add texture_star

		world_camera.reset_height 1080.0
		ui_camera.reset_height 1080.0

		setup_ui

		game.setup
	end

	fun setup_ui
	do
		app.sprites.clear
		ui_sprites.clear
		buttons.clear
		power_rating_bolts.clear
		tech_level_stars.clear

		var x = 32.0
		var y = 64.0
		var scale = 0.75

		var s: Button = new FreeLotButton(texture_bulldozer,
			ui_camera.bottom_left.offset(x, y, 0.0))
		s.scale = scale
		ui_sprites.add s
		buttons.add s

		# Rooms button
		for tech in [null: nullable Tech] + game.story.techs do
			for rule in game.story.rules do if rule isa RoomRule and not rule.static and rule.tech == tech then
				x += 64.0
				if buttons.length % 2 == 0 then
					y += (92.0 + 12.0)*scale
					x = 32.0
				end

				s = new RoomRuleButton(rule.texture, ui_camera.bottom_left.offset(x, y, 0.0), rule)
				if rule.tech != null then s.alpha *= 0.25
				s.scale = scale
				ui_sprites.add s
				buttons.add s
			end
		end

		# Power
		for i in 8.times do
			var bolt = new Sprite(texture_bolt, ui_camera.top_left.offset(200.0 + i.to_f*16.0, -64.0, 0.0))
			bolt.scale = 0.7
			bolt.tint[0] = 0.0
			bolt.tint[1] = 0.0
			bolt.tint[2] = 0.0
			ui_sprites.add bolt
			power_rating_bolts.add bolt
		end

		# Tech level
		for i in game.story.techs.length.times do
			var star = new Sprite(texture_star, ui_camera.top_left.offset(24.0 + i.to_f*32.0, -64.0, 0.0))
			star.tint[0] = 0.0
			star.tint[1] = 0.0
			star.tint[2] = 0.0
			ui_sprites.add star
			tech_level_stars.add star
		end

		# O2
		ui_sprites.add icon_o2
		icon_o2.scale = 0.7
	end

	redef fun update(dt)
	do
		super

		if not paused then
			var turn = game.update(dt*game_speed)

			# Show transactions of the last day
			if turn.new_day then
				# Categorize transactions
				var history = game.station.cash.history
				var categorised = new DefaultMap[String, Int](0)
				var total = 0
				for h in history do
					#var from = h.from
					#if from == null then
						#from = "Other"
					#else from = from.to_s

					var reason = h.reason
					if reason == null then
						reason = "Other"
					else reason = reason.to_s
					categorised[reason] += h.amount
					total += h.amount
				end
				history.clear

				# Form text
				var total_label = "Daily total"
				var first_column_width = total_label.to_s.length
				for k in categorised.keys do first_column_width = first_column_width.max(k.to_s.length)

				var lines = new Array[String]
				for k, v in categorised do
					lines.add "{k.justify(first_column_width, 1.0)} ${v}"
				end
				alpha_comparator.sort lines
				lines.add(" "*(first_column_width-1) + "---")
				lines.add "{total_label.justify(first_column_width, 1.0)} ${total}"

				# Draw
				var text = lines.join("\n")
				app.feedback text
			end
		end

		var camera_scroll_speed = 2000.0*dt
		for key in app.pressed_keys do
			if key == "w" or key == "up" then
				world_camera.position.y += camera_scroll_speed
			else if key == "s" or key == "down" then
				world_camera.position.y -= camera_scroll_speed
			else if key == "a" or key == "left" then
				world_camera.position.x -= camera_scroll_speed
			else if key == "d" or key == "right" then
				world_camera.position.x += camera_scroll_speed
			end
		end
		world_camera.position.x = world_camera.position.x.clamp(-2000.0, 2000.0)
		world_camera.position.y = world_camera.position.y.clamp(0.0, 4000.0)
		world_camera.position.z = world_camera.position.z.clamp(60.0, 5000.0)

		# The world is really flat, reset the camera far/near to fit it
		# TODO use a larger slice when we have other objects
		world_camera.near = world_camera.position.z - 32.0
		world_camera.far = world_camera.position.z + 32.0
		#print "--- {world_camera.near} {world_camera.far}"

		# Status
		sprites_status.text = "{game.station.cash.to_s_smooth}, {game.station.people.length} pop, {(game.station.happiness*100.0).round.to_i}% happy"

		# Tech level
		sprites_goals.text = game.station.goals_text

		var s = 0
		var tech_level = game.station.tech_level.order
		for star in tech_level_stars do
			if s <= tech_level then
				star.tint[0] = 1.0
				star.tint[1] = 1.0
			else
				star.tint[0] = 0.0
				star.tint[1] = 0.0
			end
			s += 1
		end

		# Power
		s = 0
		var pr = game.station.power.rating.units_smooth
		var color = game.station.power.rating.color(pr)
		for bolt in power_rating_bolts do
			if (pr*4.0+4.0).to_i <= s then
				for i in 3.times do bolt.tint[i] = 0.0
			else
				bolt.tint.clear
				bolt.tint.add_all color
			end
			s += 1
		end

		# O2
		var d = (game.station.oxygen.supply.units - game.station.oxygen.demand.units).round.to_i
		sprites_o2.text = if d > 0 then "+" + d.to_s else d.to_s
		color = game.station.oxygen.rating.color
		for i in 3.times do icon_o2.tint[i] = color[i]
	end

	redef fun accept_event(event)
	do
		if super then return true

		if world_camera.accept_scroll_and_zoom(event) then return true

		if event isa KeyEvent and event.name == "escape" then
			exit 0
		else if event isa KeyEvent and event.is_down then
			var key = event.name
			if key == "space" then
				paused = not paused
			else if key == "." then
				mod_game_speed 2.0
			else if key == "," then
				mod_game_speed 0.5
			else if key == "c" then
				game.story.worker.spawn(game.station, game.station.lot(0, 0))
			else if key == "k" then
				save_game
			else if key == "l" then
				load_game
			end
		else if event isa PointerEvent and (event.pressed or event.is_move) then

			for s in over_sprites do sprites.remove s
			over_sprites.clear

			var p = world_camera.camera_to_world(event.x, event.y)

			var rule = selected_room_rule
			if rule != null then
				# Get the bottom-left lot of the centered room
				var x = (p.x/lot_width - (rule.width - 1).to_f / 2.0).round.to_i
				var y = (p.y/lot_height - (rule.height - 1).to_f / 2.0).round.to_i
				var lot = game.station.lot(x, y)
				self.last_over_lot = lot
				var can = rule.try_spawn(lot)

				# Room ghost
				for l in can.lots do
					var s = new Sprite(rule.texture,
						new Point3d[Float](l.screen_x, l.screen_y, 0.0))
					s.alpha = 0.5
					if not can.is_ok then
						s.tint[1] = 0.5
						s.tint[2] = 0.5
					end
					app.sprites.add s
					over_sprites.add s
				end

				# Reachable warning
				if can.is_ok and rule.walkable and not can.reachable_from_terminal then
					var s = new Sprite(app.texture_warning,
						new Point3d[Float](lot.screen_x, lot.screen_y, 1.0))
					s.tint[1] = 0.0
					s.tint[2] = 0.0
					s.scale = 0.5

					app.sprites.add s
					over_sprites.add s
				end
				return true
			end

			# If destroy is selected, make destroyable rooms redish
			var x = (p.x/lot_width).round.to_i
			var y = (p.y/lot_height).round.to_i
			var lot = game.station.lot(x, y)
			var room = lot.room
			if selected_free and room != null then
				var can = room.can_destroy
				for l in room.lots do
					var s = new Sprite(room.rule.texture,
						new Point3d[Float](l.screen_x, l.screen_y, 0.0))
					s.tint[1] = 0.5
					s.tint[2] = 0.5
					if not can then s.tint[0] = 0.5
					app.sprites.add s
					over_sprites.add s
				end
			end
		else if event isa PointerEvent and event.depressed and not event.is_move then
			for s in over_sprites do sprites.remove s
			over_sprites.clear

			for but in buttons do
				if but.hit(event) then return true
			end

			var p = world_camera.camera_to_world(event.x, event.y)

			var rule = selected_room_rule
			if rule != null then
				# Get the bottom-left lot of the centered room
				var x = (p.x/lot_width - (rule.width - 1).to_f / 2.0).round.to_i
				var y = (p.y/lot_height - (rule.height - 1).to_f / 2.0).round.to_i
				var lot = game.station.lot(x, y)

				# Add an elevator car?
				var room = lot.room
				if room isa Elevator and room.rule == rule and room.can_buy_car then
					room.buy_car lot
					return true
				end

				var can = rule.try_spawn(lot)
				if can.is_ok then can.spawn
				return true
			end

			var x = (p.x/lot_width).round.to_i
			var y = (p.y/lot_height).round.to_i
			var lot = game.station.lot(x, y)
			if selected_free then
				var room = lot.room
				if room != null and room.can_destroy then
					room.destroy
				end
			else
				lot.dump

				# Show path between last and current lot
				var last_lot = self.last_lot
				if last_lot != null then
					var path = last_lot.path_to(lot)
					if path != null then
						print "path from {last_lot} to {lot}: {path.plan}"
					else
						print "not path from {last_lot} to {lot}"
					end
				end
				self.last_lot = lot
			end

			#sprites.first.center.x = p.x
			#sprites.first.center.y = p.y
			#sprites.first.tint[2] = 1.0.rand
			#sprites.first.tint[1] = 1.0.rand
		else if event isa QuitEvent then
			exit 0
		end

		return false
	end

	# The last lot clicked in free mode
	var last_lot: nullable Lot = null

	fun save_game
	do
		#game.serialize_to_json(pretty=true, plain=true)
		var stream = new FileWriter.open(save_path)
		var engine = new BinarySerializer(stream)
		engine.serialize game
		stream.close

		feedback "Game saved to '{save_path}'"
	end

	fun load_game
	do
		var stream = new FileReader.open(save_path)
		var engine = new BinaryDeserializer(stream)
		var game = engine.deserialize
		stream.close

		if engine.errors.not_empty then
			feedback "Deserialization errors:\n" + engine.errors.join("\n")
		else if game == null then
			feedback "Deserialization error: got `null`"
		else if not game isa Game then
			feedback "Deserialization error: got a `{game.class_name}`"
		else
			# Set as current game
			self.game = game

			# Load missing textures
			# FIXME reuse previously loaded textures
			game.story.load_assets
			for texture in all_root_textures do texture.load

			# Update display
			setup_ui
			for c in game.station.people do c.display
			for r in game.station.rooms do r.display

			# Notify user
			feedback "Game loaded"
		end
	end

	fun feedback(text: Text)
	do
		app.sprites_describe.anchor.x = app.ui_camera.top_right.x - 16.0 - app.font.text_width(text).to_f
		app.sprites_describe.text = text
	end
end

redef class RoomAttempt
	redef fun is_ok do return super and reachable_or_dont_care

	# Will this room be reachable or `not ClientConfig::build_reachable_only`?
	fun reachable_or_dont_care: Bool
	do return not app.config.build_reachable_only or not rule.walkable or reachable_from_terminal or extends != null
end

redef class Station
	redef fun tech_level=(lvl)
	do
		super

		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule.tech == lvl then
				but.alpha /= 0.25
			end
		end
	end
end

redef class Port
	var car_sprite: Sprite is lazy do
		var s = new Sprite(rule.ship_texture, new Point3d[Float](screen_x, screen_y, car_z))
		app.sprites.add s
		return s
	end

	fun car_z: Float do return 8.0

	redef fun update(turn)
	do
		super

		var p
		if next_event_is_arrival then
			var time_left = next_event_time - turn.time
			p = time_left / (rule.period_out * turn.game.day_length) - 0.5
			p *= -2.0
			# p in [-1.0..1.0] when -1.0 means almost there
			if p < 0.0 then
				# leaving
				p = 1.0 + p
			else p = p - 1.0
		else p = 0.0
		update_car_position(turn, p)
	end

	# Update position of `car_sprite` at `progress` in [-1.0..1.0] where 0.0 is docked and -1.0 is coming in
	fun update_car_position(turn: Turn, progress: Float) do end

	redef fun on_arrival(turn)
	do
		super
		wide_sprite.texture = rule.wide_texture_opened
	end

	redef fun on_departure(turn)
	do
		super
		wide_sprite.texture = rule.wide_texture_closed
	end

	redef fun on_destroy
	do
		super
		app.sprites.remove car_sprite
	end
end

redef class BuyablePort

	redef fun car_z do return -8.0

	private var ship_vector_a: Float = 2.0*pi.rand
	private var ship_vector_x: Float = ship_vector_a.cos
	private var ship_vector_y: Float = ship_vector_a.sin
	private var ship_vector_d: Float = 12000.0 & 2000.0

	redef fun update_car_position(turn, p)
	do
		var d = ship_vector_d
		var dirx = ship_vector_x/ship_vector_x.abs
		var ox = car_sprite.center.x
		var oy = car_sprite.center.y

		var handle
		if p < 0.0 then
			# Coming in
			handle = screen_x + dirx * 2000.0
		else handle = screen_x - dirx * 3000.0

		p = p.pow(2.0)
		car_sprite.center.x = p.qerp(screen_x, handle, screen_x + d * ship_vector_x)
		car_sprite.center.y = p.qerp(screen_y, screen_y, screen_y + d * ship_vector_y)

		if p != 0.0 then
			car_sprite.rotation = -atan2(car_sprite.center.x - ox, car_sprite.center.y - oy) + pi/2.0
		end
	end
end

redef class SpaceElevator

	redef fun update_car_position(turn, p)
	do
		var y = screen_y - 0.13 * lot_height
		car_sprite.center.y = -p.pow(2.0) * 5000.0 + y
	end
end

redef class ElevatorCar
	var car_sprite: Sprite is lazy do
		var index = elevator.cars.index_of(self)
		var s = new Sprite(app.texture_elevator_car,
			new Point3d[Float](elevator.lots.first.screen_x, elevator.lots.first.screen_y, 4.0 + index.to_f * 0.5))
		app.sprites.add s

		var n = 0
		for c in elevator.cars do
			if c == self then break
			n += 1
		end

		var c = 1.0 - n.to_f * 0.2
		for i in 3.times do s.tint[i] = c
		return s
	end

	redef fun update(turn)
	do
		super

		car_sprite.center.y = y.to_f * lot_height - 16.0
	end
end

redef class Elevator
	redef fun on_join_lot(lot)
	do
		var olds = lot.sprite
		if olds != null then app.sprites.remove olds
		var s = new Sprite(rule.texture,
			new Point3d[Float](lot.screen_x, lot.screen_y, 0.0))
		lot.sprite = s
		app.sprites.add s
	end

	redef fun destroy
	do
		super

		for car in cars do
			app.sprites.remove car.car_sprite
		end
	end
end

class Button
	super Sprite

	fun hit(event: PointerEvent): Bool
	do
		var ui_pos = app.ui_camera.camera_to_ui(event.x, event.y)
		var dx = (ui_pos.x - center.x).abs
		var dy = (ui_pos.y - center.y).abs

		var r = dx < 32.0 and dy < 48.0
		if r then on_click
		return r
	end

	protected fun on_click do act

	fun act do print "Action!"
end

class RoomRuleButton
	super Button

	var room_rule: RoomRule

	redef fun act
	do
		if app.selected_free then app.buttons.first.tint[2] /= 0.25
		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule == app.selected_room_rule then
				but.tint[2] /= 0.25
			end
		end
		app.selected_free = false

		if app.selected_room_rule == room_rule then
			app.selected_room_rule = null
			app.sprites_describe.text = null
			return
		end

		app.selected_room_rule = room_rule

		var desc = room_rule.describe
		app.feedback desc
		tint[2] *= 0.25
	end
end

class FreeLotButton
	super Button

	init do scale = 64.0 / texture.width

	redef fun act
	do
		if app.selected_free then app.buttons.first.tint[2] /= 0.25
		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule == app.selected_room_rule then
				but.tint[2] /= 0.25
			end
		end
		app.selected_room_rule = null

		app.selected_free = not app.selected_free
		if app.selected_free then tint[2] *= 0.25
	end
end

redef class RoomRule

	# Tint when in use
	var use_tint: Array[Float] is lazy do
		if roles.count(story.leisure) > 0 then return [0.5, 0.5, 1.0, 1.0]
		if roles.count(story.work) > 0 then return [0.2, 0.5, 1.0, 1.0]
		if roles.count(story.residence) > 0 then return [0.5, 0.8, 0.5, 1.0]
		return [1.0, 1.0, 1.0, 1.0]
	end
end

redef class Room
	# Related sprites incl. furniture, but not the lots sprites
	var sprites = new Array[Sprite]

	# Single sprite covering the whole background, replaces lot sprites
	var wide_sprite: nullable Sprite = null

	fun show_furniture_by_default: Bool do return true

	redef fun on_spawn
	do
		super
		display
	end

	fun display
	do
		var wide_texture = rule.wide_texture
		if wide_texture != null then
			var s = new Sprite(wide_texture,
				new Point3d[Float](screen_x, screen_y, 0.0))
			wide_sprite = s
			app.sprites.add s
		else
			for lot in lots do
				var olds = lot.sprite
				if olds != null then app.sprites.remove olds
				var s = new Sprite(rule.texture,
					new Point3d[Float](lot.screen_x, lot.screen_y, 0.0))
				lot.sprite = s
				app.sprites.add s
			end
		end

		if show_furniture_by_default then display_furniture
	end

	fun display_furniture
	do
		if rule.furniture.not_empty then
			var not_used_furniture = rule.furniture.to_a
			var min_x = lots.first.screen_x
			var width = (rule.width-1).to_f * 64.0

			# Add random furniture
			for i in rule.furniture_per_room.times do
				var furniture
				if not_used_furniture.not_empty then
					furniture = not_used_furniture.rand
					not_used_furniture.remove furniture
				else furniture = rule.furniture.rand

				var x = min_x + width.rand
				var y = lots.first.screen_y
				var z = [3.0, 6.0, 14.0, 17.0].rand
				var s = new Sprite(furniture,
				                   new Point3d[Float](x, y, z))
				self.sprites.add s
				app.sprites.add s
			end
		end
	end

	redef fun on_destroy
	do
		super

		var wide_sprite = wide_sprite
		if wide_sprite != null then app.sprites.remove wide_sprite

		for lot in lots do
			var s = lot.sprite
			if s != null then app.sprites.remove s
		end

		app.sprites.remove_each sprites
	end

	fun screen_x: Float
	do
		var sum = 0.0
		for lot in lots do sum += lot.screen_x
		return sum / lots.length.to_f
	end

	fun screen_y: Float
	do
		var sum = 0.0
		for lot in lots do sum += lot.screen_y
		return sum / lots.length.to_f
	end
end

redef class Lot
	var sprite: nullable Sprite = null

	fun screen_x: Float do return x.to_f * lot_width
	fun screen_y: Float do return y.to_f * lot_height
end

redef class Character
	var sprite = new Sprite(app.characters_spritesheet.characters.rand.standing,
		new Point3d[Float](x, y, 10.0))

	init do display

	fun display
	do
		sprite.scale = 0.3
		app.sprites.add sprite
	end

	redef fun leave_station(turn)
	do
		super
		app.sprites.remove sprite
	end

	redef fun x=(x)
	do
		super
		sprite.center.x = x.to_f * lot_width

		var dy = y - y.floor
		if dy != 0.0 then
			# Stairs
			var w = 40.0
			var dx
			if dy < 0.66 then
				dx = -w*0.5 + dy * w / 0.66
			else dx = w*0.5 - (dy-0.66) * w / 0.33
			sprite.center.x += dx
		end
	end

	redef fun y=(y)
	do
		super
		sprite.center.y = y.to_f * lot_height - 18.0
	end
end

redef class Characters_SpritesheetImages
	var characters = new Array[CharacterSpritesheet].with_items(
		new CharacterSpritesheet(green_standing),
		new CharacterSpritesheet(blue_standing),
		new CharacterSpritesheet(pink_standing),
		new CharacterSpritesheet(yellow_standing))
		#new CharacterSpritesheet(gray_standing)) it doesn't fit the current style
end

class CharacterSpritesheet
	var standing: Texture
	#var walk: Array[Texture] TODO
end

redef class BuyableRoom

	redef fun show_furniture_by_default do return false

	redef fun display
	do
		super
		if bought then
			display_furniture
		else
			var s = new Sprite(app.texture_for_sale,
				new Point3d[Float](screen_x, screen_y, 2.0))
			sprites.add s
			app.sprites.add s
		end
	end

	redef fun buy(turn)
	do
		super

		app.sprites.remove_each sprites
		sprites.clear

		display_furniture
	end
end

redef class RentableRoom

	redef fun show_furniture_by_default do return false

	redef fun display
	do
		super
		if rented then
			display_furniture
		else
			var s = new Sprite(app.texture_for_rent,
				new Point3d[Float](screen_x, screen_y, 2.0))
			sprites.add s
			app.sprites.add s
		end
	end

	redef fun start_renting(turn)
	do
		super

		app.sprites.remove_each sprites
		sprites.clear

		display_furniture
	end

	redef fun stop_renting
	do
		super
		app.sprites.remove_each sprites
		sprites.clear

		var s = new Sprite(app.texture_for_rent,
			new Point3d[Float](screen_x, screen_y, 2.0))
		sprites.add s
		app.sprites.add s
	end
end

redef class FluidRating
	fun color(rating: nullable Float): Array[Float]
	do
		rating = rating or else units_smooth
		return if rating > 0.5 then
			[0.0, 1.0, 0.0, 1.0] # green
		else if rating >= 0.0 then
			[1.0, 1.0, 0.0, 1.0] # yellow
		else [1.0, 0.0, 0.0, 1.0] # red
	end
end
