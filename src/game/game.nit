module game is serialize

import base
import room
import path
import economy
import characters
import space_elevator
import elevator
import tech
import power
import oxygen
import describe
import event
import defense

redef class Port
	# Spawn a character at each arrival
	redef fun on_arrival(turn)
	do
		super

		# Bring up some of the pop needed to fill all roles (according to happiness levels)
		var work = turn.game.story.work
		var residence = turn.game.story.residence

		var available_roles = 0
		for r in station.rooms do if r.reachable_from_terminal then
			available_roles += r.available_roles[work]
			available_roles += r.available_roles[residence]
		end
		available_roles = available_roles/2

		var new_chars = chars_to_spawn(available_roles)
		for i in [0..new_chars[ do
			turn.game.story.worker.spawn(station, spawn_lots.rand)
		end
	end

	# Take away characters without a residence and workplace
	redef fun on_departure(turn)
	do
		super

		var work = turn.game.story.work
		var residence = turn.game.story.residence

		var terminal_lots = station.rooms.first.lots
		for c in station.people.to_a do
			if terminal_lots.has(c.current_lot) and
			   (c.roles[work] == null and c.roles[residence] == null) then
				c.leave_station turn
			end
		end
	end

	# Produce the number of `Character` to spawn at an arrival
	#
	# This is determined using the average number of `available_roles`
	# and `station.happiness`.
	fun chars_to_spawn(available_roles: Int): Int
	do
		var mod_roles = available_roles.to_f * station.happiness
		return (mod_roles / 8.0).ceil.to_i.min(rule.max_influx)
	end
end

redef class Station
	# Global happiness level of the population
	#
	# Set from the fulfillment of all the roles for each characters.
	# Affects the number of immigrants at each space elevator arrival.
	var happiness = 0.75

	redef fun update(turn)
	do
		super

		# Reserve a residence and a workplace to all characters
		var residence = turn.game.story.residence
		var work = turn.game.story.work
		var leisure = turn.game.story.leisure

		var roles_to_assign = [residence, work, leisure]

		# Once a day, reset the assigned leisure
		if turn.new_day then
			for character in people do character.leave_role leisure
		end

		# Count characters missing a `Role`
		var missing = new DefaultMap[Role, Int](0)

		# Assign roles
		for role in roles_to_assign do
			var available = new Array[Room]
			for r in rooms do if r.available_roles[role] > 0 then
				if r.reachable_from_terminal and (role != leisure or r.assigned_roles[work].not_empty) then
					available.add r
				end
			end

			for c in people do if c.roles[role] == null then
				if available.is_empty then
					missing[role] += 1
					continue
				end

				var r = available.rand
				c.take_role(turn, role, r)

				if role != leisure then
					if r isa BuyableRoom and not r.bought then r.buy turn
					if r isa RentableRoom and not r.rented then r.start_renting turn
				end

				if r.available_roles[role] == 0 then available.remove r
			end
		end

		if turn.new_day and people.not_empty then
			var missing_sum = missing[work] + missing[residence] + missing[leisure]
			var problems = missing_sum.to_f/people.length.to_f/3.0
			problems = problems.sqrt
			var happiness_target = 1.0 - problems

			# Smooth happiness changes
			happiness += (happiness_target-happiness)/2.5
		end
	end
end

redef class Character
	redef fun leave_role(role)
	do
		# Stop renting if last
		var room = roles[role]
		if role.name != "Leisure" and room isa RentableRoom then
			if room.assigned_roles[role].length == 1 then
				room.stop_renting
			end
		end

		super
	end
end

redef class Game
	# Accept any linearisation and silence warning
	redef fun setup do super
end

redef class RoomAttempt
	# Accept any linearisation and silence warning
	redef fun is_ok do return super
end
