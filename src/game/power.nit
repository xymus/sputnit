module power is serialize

import base
import room
import characters
import fluid_resources

redef class Station
	var power = new FluidResource

	# Time of day to collect power import cost
	private var next_power_bill = 0.0

	redef fun update(turn)
	do
		super

		var billing_period = 0.1
		var power_cost_per_day = 25.0
		var power_unit_cost = power_cost_per_day * billing_period

		while next_power_bill < turn.game.time / turn.game.day_length do
			# Power cost from the ground when in deficit
			var deficit = power.demand.units - power.supply.units
			if deficit > 0.0 then
				#print "power cost: {deficit * power.unit_cost}"
				var c = (deficit * power_unit_cost).round.to_i # TODO use floats
				cash.dec(c, self, "Power import")
			end
			next_power_bill += billing_period
		end
	end
end

redef class Room
	redef fun on_spawn
	do
		var p = rule.power
		if p > 0.0 then
			station.power.supply.units += p
		else station.power.demand.units -= p

		super
	end

	redef fun destroy
	do
		var p = rule.power
		if p > 0.0 then
			station.power.supply.units -= p
		else station.power.demand.units += p

		super
	end
end

redef class Character
	redef fun take_role(turn, role, room)
	do
		super
		var p = room.rule.power_per_roles
		if p > 0.0 then
			room.station.power.supply.units += p
		else room.station.power.demand.units -= p
	end

	redef fun leave_role(role)
	do
		var room = roles[role]
		if room != null then
			var p = room.rule.power_per_roles
			if p > 0.0 then
				room.station.power.supply.units -= p
			else room.station.power.demand.units += p
		end

		super
	end
end
