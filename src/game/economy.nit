module economy is serialize

import room
import resource

redef class Room
	redef fun update(turn) do rule.update(turn, self)

	redef fun on_spawn
	do
		station.cash.dec(rule.cost, rule, "Construction")
		super
	end

	redef fun on_destroy
	do
		station.cash.inc(rule.refund, rule, "Refund")
		super
	end
end

# Room than can be bought once
class BuyableRoom
	super Room

	redef type RULE: BuyableRoomRule

	# Has this room been bought?
	var bought = false

	# Character buys this room
	fun buy(turn: Turn)
	do
		bought = true
		station.cash.inc(rule.one_time_cost, rule, "Room sale")
	end

	# TODO make available again if the owner dies
	# Character sell back this room (on death or rare occasion)
	#fun sell(turn: Turn)
	#do
	#	bought = false
	#	station.cash.dec rule.one_time_cost/2
	#end

	redef fun on_destroy
	do
		super
		if bought then station.cash.dec(rule.one_time_cost, rule, "Room buyback")
	end
end

# Room that can be rented as needed
class RentableRoom
	super Room

	redef type RULE: RentableRoomRule

	# Is it currently rented by tenants?
	var rented = false

	fun start_renting(turn: Turn) do rented = true

	fun stop_renting do rented = false
end

redef class RoomAttempt
	# The cost of the room
	var cost: Int = 0

	# Is there enough resources to build? (mandatory)
	var has_ressources = true

	redef fun is_ok do return super and has_ressources
end

redef class RoomRule
	fun update(turn: Turn, room: Room)
	do
		if turn.new_day then room.station.cash.dec(daily_cost, self, "Maintenance")
	end

	redef fun try_spawn(lot) do
		var result = super
		result.cost = cost
		if not lot.station.cash.has(cost) then result.has_ressources = false
		return result
	end
end

class BuyableRoomRule
	super RoomRule

	var one_time_cost = 200 is optional

	redef fun new_room(station) do return new BuyableRoom(self, station)
end

class RentableRoomRule
	super RoomRule

	var daily_rent = 10 is optional

	redef fun new_room(station) do return new RentableRoom(self, station)

	redef fun update(turn, room)
	do
		assert room isa RentableRoom
		if room.rented and turn.new_day then room.station.cash.inc(daily_rent, self, "Rent")
	end
end

redef class EmptyRoom
	redef fun refund do return 0
end

redef class Game
	redef fun setup
	do
		super
		station.cash.inc(10000, "Investors", "Initial cash")
	end
end
