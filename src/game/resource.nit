module resource is serialize

import base

class Resource
	var units = 0 is writable
	var max = 50_000 is writable
	fun name: Text is abstract
	fun symbol: Text is abstract

	# Transaction history (does not take into account when `max` is reached)
	var history = new Array[ResourceHistoryEntry]

	# Does `self` has enough units to cover for the cost of `amount`
	fun has(amount: Int): Bool do return amount <= units

	# Add `u` units, `from` a source for a `reason`
	fun inc(u: Int, from: nullable Object, reason: nullable Text): Resource do
		units += u
		units = units.clamp(0, max)
		if u != 0 then history.add new ResourceHistoryEntry(u, from, reason)
		return self
	end

	# Remove `u` units, `from` a source for a `reason`
	fun dec(u: Int, from: nullable Object, reason: nullable Text): Resource do
		units -= u
		units = units.clamp(0, max)
		if u != 0 then history.add new ResourceHistoryEntry(-u, from, reason)
		return self
	end

	# Attenuated over time version of `units` for the UI
	#
	# We don't really care about dt at this point,
	# so each call act as a step.
	fun units_smooth: Int
	do
		var s = 16.0
		var d = units.to_f - units_last

		var u
		if d.abs < 0.5 then
			u = units.to_f
		else u = units_last + d/s
		units_last = u

		return u.to_i
	end

	private var units_last: Float = units.to_f is lazy

	redef fun to_s do return "{symbol}{units}/{max}"

	# UI version of `to_s` using `units_smooth`
	fun to_s_smooth: String do return "{symbol}{units_smooth}/{max}"
end

class Water
	super Resource
	redef var name = "Water"
	redef var symbol = "💧"
end

class Cash
	super Resource
	redef var name = "Cash"
	redef var symbol = "$"
end

class Food
	super Resource
	redef var name = "Food"
	redef var symbol = "🍗"
end

redef class Station
	var water = new Water
	var cash = new Cash
	var food = new Food
end

class ResourceHistoryEntry
	var amount: Int
	var from: nullable Object
	var reason: nullable Text
end
