module elevator is serialize

import geometry
private import ai::search

intrude import room
import characters

class WaitingLine
	var elevator: Elevator

	var deck: Int

	var characters = new Array[Character]

	fun add(character: Character)
	do
		characters.add character
		if characters.length == 1 then elevator.activate_line self
	end

	fun pop(n: Int): Array[Character]
	do
		var a = new Array[Character]
		while characters.not_empty do a.add characters.shift
		return a
	end

	fun direction: Int
	do
		for dir, v in elevator.lines do for deck, line in v do if line == self then return dir
		abort
	end
end

class ElevatorRule
	super RoomRule

	var car_speed = 2.0 is optional

	# Number of characters in each `ElevatorCar`
	var car_capacity = 4 is optional

	# Number of `ElevatorCars` in the elevator
	var max_cars = 4 is optional

	redef fun new_room(station) do return new Elevator(self, station)

	redef fun spawn_impl(bl)
	do
		var room = super
		assert room isa Elevator
		room.add_car
		room.add_car
		return room
	end
end

class Elevator
	super Room

	redef type RULE: ElevatorRule

	# Lines per orientation (-1 & 1) then level
	var lines: Map[Int, Map[Int, WaitingLine]] do
		var lines = new Map[Int, Map[Int, WaitingLine]]
		lines[-1] = new LineMap(self)
		lines[ 1] = new LineMap(self)
		return lines
	end

	# Lines with people in it (before entering a car)
	var lines_active = new Set[WaitingLine]

	# Active `ElevatorCars`
	var cars = new Array[ElevatorCar]

	# Cars without a target line
	private var idle_cars = new Set[ElevatorCar]

	fun can_buy_car: Bool
	do
		return station.cash.has(rule.cost) and cars.length < rule.max_cars
	end

	fun buy_car(lot: nullable Lot): ElevatorCar
	do
		station.cash.dec(rule.cost, self, "Construction")
		return add_car(lot)
	end

	fun add_car(lot: nullable Lot): ElevatorCar
	do
		lot = lot or else lots.first
		var car = new ElevatorCar(self, lot.y.to_f)
		cars.add car
		idle_cars.add car
		car_available car
		return car
	end

	fun car_available(car: ElevatorCar)
	do
		idle_cars.add car
		dispatch
	end

	fun activate_line(line: WaitingLine)
	do
		lines_active.add line
		dispatch
	end

	fun dispatch
	do
		# TODO notify moving cars with pickups on their way

		if idle_cars.not_empty and lines_active.not_empty then
			# Dispatch cars
			var line = lines_active.first
			lines_active.remove line

			# Find nearest car
			var cars = idle_cars.to_a
			(new CarComparator(line.deck)).sort(cars)
			var car = cars.shift
			idle_cars.remove car

			car.target_pickup = line
		end
	end

	redef fun update(turn)
	do
		super

		for car in cars do car.update turn
	end

	fun on_join_lot(lot: Lot) do end
end

class LineMap
	super HashMap[Int, WaitingLine]

	var elevator: Elevator

	redef fun provide_default_value(key)
	do
		if not key isa Int then return super
		var line = new WaitingLine(elevator, key)
		self[key] = line
		return line
	end
end

class ElevatorCar
	super Turnable

	var elevator: Elevator

	var y: Float

	var target_pickup: nullable WaitingLine = null

	var target_decks = new Array[Int]

	var occupants = new Array[Character]

	redef fun update(turn)
	do
		super

		var target_pickup = target_pickup
		var target_decks = target_decks
		if target_pickup != null then
			#print "Go to pickup"

			var target_y = target_pickup.deck
			if step_towards(turn, target_y) then
				# At destination
				#print "Pickup"
				self.target_pickup = null

				# Pick up
				var cap = elevator.rule.car_capacity
				while cap > 0 do
					if target_pickup.characters.is_empty then break
					cap -= 1

					var c = target_pickup.characters.shift
					c.in_line = null
					c.in_car = self
					occupants.add c
					target_decks.add c.target_y
				end
				if target_pickup.characters.not_empty then elevator.activate_line target_pickup

				if occupants.length < elevator.rule.car_capacity and elevator.lines_active.not_empty then
					# Target a next pickup
					var lines = elevator.lines_active.to_a
					(new LineComparator(y.to_i)).sort(lines)
					var line = lines.first
					self.target_pickup = line
					elevator.lines_active.remove line
				end

				(new DeckComparator(y)).sort(target_decks)
			end

			#print "car: {y} {target_y}"
		else if target_decks.not_empty then
			#print "Go to drop"
			var target_y = target_decks.first #default_comparator.sort(target_decks).unshift
			if step_towards(turn, target_y) then
				#print "Drop off"
				target_decks.shift

				# Drop off
				for occ in occupants.reversed do
					if occ.target_y == y.to_i then
						occ.in_car = null
						occ.force_y = occ.target_y.to_f
						occ.force_x = elevator.lots.first.x.to_f & 0.25
						occupants.remove occ

						var plan = occ.current_plan.plan
						while plan.not_empty do
							if plan.first.x == elevator.lots.first.x then
								plan.shift
							else break
						end
					end
				end

				if target_decks.is_empty then elevator.car_available self
			end
		end

		# TODO move chars too
	end

	private fun step_towards(turn: Turn, target_y_i: Int): Bool
	do
		var target_y = target_y_i.to_f
		var dy = target_y - y
		var oy = if dy != 0.0 then dy / dy.abs else 0.0
		#print "step from {y} to {target_y} - {oy}"

		var y = y + elevator.rule.car_speed * oy * turn.dt
		if oy == 0.0 or (oy == 1.0 and y >= target_y) or (oy == -1.0 and y <= target_y) then
			self.y = target_y
			return true
		else
			self.y = y
			return false
		end
	end
end

redef class Character
	var in_line: nullable WaitingLine = null

	var in_car: nullable ElevatorCar = null

	redef fun update(turn)
	do
		var cs = current_lot
		var room = cs.room
		if room isa Elevator then
			var line = in_line
			var car = in_car
			if car != null then
				#print "-- {name} in car"
				y = car.y
			else if line != null then
				#print "-- {name} in line"
				# TODO move to a real line
			else if target_y == y.to_i then
				#print "-- {name} right deck"
				super
			else
				# Get in line
				#print "-- {name} get in line"
				var t = null
				var o = 1
				var x = cs.x
				var n = 0
				for s in current_plan.plan do
					if s.x != x then
						n += 1
						t = s.y
						o = s.y - cs.y
						if o != 0 then o = o / o.abs
						break
					end
				end
				assert t != null
				if o == 0 then
					super
				else
					line = room.lines[o][y.round.to_i]
					line.add self
					self.in_line = line

					self.x = cs.x.to_f & 0.4
				end
			end
			return
		end

		super
	end

	# Next deck on the path
	private fun target_y: Int
	do
		var cs = current_lot
		var t = null
		var x = cs.x
		for s in current_plan.plan do
			if s.x != x then
				t = s.y
				break
			end
		end
		assert t != null
		return t
	end

	private fun force_y=(y: Float) do self.y = y
	private fun force_x=(x: Float) do self.x = x
end

class CarComparator
	super Comparator

	var y: Int
	private var yf: Float = y.to_f is lazy

	redef type COMPARED: ElevatorCar

	redef fun compare(a, b) do
		var d = ((a.y - yf).abs - (b.y - yf).abs)
		return (d*100.0).to_i
	end
end

class DeckComparator
	super Comparator

	var y: Float

	redef type COMPARED: Int

	redef fun compare(a, b) do
		var d = ((a.to_f - y).abs - (b.to_f - y).abs)
		return (d*100.0).to_i
	end
end

class LineComparator
	super Comparator

	var y: Int

	redef type COMPARED: WaitingLine

	redef fun compare(a, b) do
		var d = ((a.deck - y).abs - (b.deck - y).abs)
		return d
	end
end

redef class RoomAttempt
	# Intercept spawning of new elevator to instead extend a nearby elevator
	redef fun spawn
	do
		var room = extends
		if room != null then
			# Add to existing elevator
			room.lots.add anchor
			anchor.room = room
			room.on_join_lot anchor

			# Duplicates logic from `extends`
			var lot = null
			for v in [anchor.down, anchor.up] do if v.room == room then lot = v
			assert lot != null

			anchor.add_walk lot
			anchor.left.add_walk anchor
			anchor.right.add_walk anchor
			return room
		end

		return super
	end

	# Which elevator, if any, will this extends?
	fun extends: nullable Elevator
	do
		if rule isa ElevatorRule then
			for v in [anchor.down, anchor.up] do
				var room = v.room
				if room != null and room.rule == rule and room isa Elevator then return room
			end
		end
		return null
	end
end

redef class PathProblem
	redef fun cost(from_lot, action, to_lot)
	do
		var room = from_lot.room
		if room == null then return super

		if room isa Elevator and to_lot.room isa Elevator and from_lot.y == to_lot.y then
			var in_line = 0
			for dir in room.lines.values do dir[from_lot.y.to_i].characters.length
			return 1.0 + in_line.to_f * 2.0
		end

		return super
	end
end
