module oxygen is serialize

import characters
import fluid_resources

import power # For linearisation

redef class Station
	var oxygen = new FluidResource

	private var next_oxygen_check = 0.0

	redef fun update(turn)
	do
		super

		while next_oxygen_check < turn.game.time / turn.game.day_length do
			var balance = oxygen.supply.units - oxygen.demand.units
			#print "O2 {balance}"
			if balance > 0.0 then
				# Fill tanks TODO
			else if balance < 0.0 then
				# Empty tanks TODO

				# Kill people
				print "kill!"
				people.rand.die turn
			end

			var check_period = 0.1
			next_oxygen_check += check_period
		end
	end
end

redef class Character
	init do current_lot.station.oxygen.demand.units += 1.0

	redef fun leave_station(turn)
	do
		current_lot.station.oxygen.demand.units -= 1.0
		super
	end

	redef fun take_role(turn, role, room)
	do
		super
		room.station.oxygen.supply.units += room.rule.oxygen_per_roles
	end

	redef fun leave_role(role)
	do
		super
		var room = roles[role]
		if room != null then
			room.station.oxygen.supply.units -= room.rule.oxygen_per_roles
		end
	end
end

redef class Room

	redef fun on_spawn
	do
		station.oxygen.supply.units += rule.oxygen
		super
	end

	redef fun destroy
	do
		station.oxygen.supply.units -= rule.oxygen
		super
	end
end
