module defense is serialize

import geometry

import room

class DefenseRoomRule
	super RoomRule

	var defense_radius = 0.0 is optional

	redef fun new_room(station) do return new DefenseRoom(self, station)
end

class DefenseRoom
	super Room

	redef type RULE: DefenseRoomRule

	fun open_fire(x, y: Int) do end
end
