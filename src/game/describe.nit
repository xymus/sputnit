import room
import economy
import characters
import tech

redef class Rule
	# Full description for the user
	fun describe: Text do return name
end

redef class RoomRule
	redef fun describe do return """
{{{super}}}
Build for ${{{cost}}}
Roles: {{{describe_roles}}}
{{{describe_tech}}}"""

	private fun describe_roles: String
	do
		var map = new DefaultMap[Role, Int](0)
		for r in roles do map[r] += 1
		var words = [for r in map.keys do "{map[r]} {r.name}"]
		return words.join(", ", " & ")
	end

	private fun describe_tech: String
	do
		var tech = tech
		if tech == null then return "Tech level ★"
		return "Tech level {"★"*(tech.order+1)}"
	end
end

redef class BuyableRoomRule
	redef fun describe do return """
{{{super}}}
Sells for: ${{{one_time_cost}}}"""
end

redef class RentableRoomRule
	redef fun describe do return """
{{{super}}}
Daily rent: ${{{daily_rent}}}"""
end
