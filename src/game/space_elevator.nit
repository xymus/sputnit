module space_elevator is serialize

import lib

import base
import room

class Port
	super Room

	redef type RULE: PortRule

	# Time of the next event, arrival or departure
	var next_event_time = 0.0

	# Is the next event at `next_event_time` an arrival? Otherwise it's a departure.
	var next_event_is_arrival = true

	private var just_built = true

	redef fun update(turn)
	do
		super

		if just_built then
			# FIXME use `spawn(turn)` to set the first arrival after the build
			just_built = false
			next_event_time = (turn.game.day + rule.period_out/2.0)*turn.game.day_length
		end

		if turn.time > next_event_time then
			if next_event_is_arrival then
				on_arrival turn
				next_event_time += rule.period_in * turn.game.day_length
			else
				on_departure turn
				next_event_time += rule.period_out * turn.game.day_length
				#next_event_time += (rule.period_out & rule.period_out_fuzzy) * turn.game.day_length
			end
			next_event_is_arrival = not next_event_is_arrival
		end
	end

	# Event on the car arrival to the station
	fun on_arrival(turn: Turn) do end

	# Event on the car departure from the station
	fun on_departure(turn: Turn) do end

	# TODO move up as `Room::walk_lots`
	var spawn_lots: Array[Lot] = [for l in lots do if l.walk.not_empty then l] is lazy
end

class PortRule
	super RoomRule

	# Time between departure and arrival (in prop of day_length)
	var period_out = 0.2 is optional

	# Randomization time range around `period_out`
	#var period_out_fuzzy = 0.0 # TODO breaks logic in UI

	# Time between arrival and departure (in prop of day_length)
	var period_in = 0.05 is optional

	# Max number of characters dropped off at each arrival
	var max_influx = 8 is optional

	redef fun new_room(station) do return new Port(self, station)
end

class SpaceElevator
	super Port
end

class TerminalRule
	super PortRule

	redef fun new_room(station) do return new SpaceElevator(self, station)
end
