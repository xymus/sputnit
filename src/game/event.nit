module event is serialize

import base
import room
import characters
import defense

redef class Story
	var events = new Set[EventRule] is lazy

	var hull_breach = new HullBreachRule(self, "Hull breach")
end

class EventRule
	super Rule

	init do story.events.add self

	fun new_event(turn: Turn): Event is abstract
end

abstract class Event
	super Turnable

	var station: Station
end

redef class Station
	# Current event
	var event: nullable Event = null

	# Happens about once every 5 days ± 3 days
	# TODO move to a rule?
	private var event_period = 5.0
	private var event_period_fuzzy = 3.0

	# Day at which this event happens next
	var next_occurence: Float = event_period & event_period_fuzzy

	redef fun update(turn)
	do
		super

		var event = event
		if event != null then
			event.update turn
		else
			if turn.game.day >= next_occurence then
				next_occurence = turn.game.day + (event_period & event_period_fuzzy)
				self.event = turn.game.story.events.rand.new_event(turn)
			end
		end
	end
end

# ---
# Hull breach

class HullBreachRule
	super EventRule

	redef fun new_event(turn) do return new HullBreach(turn.game.station)
end

# Hull breach event
class HullBreach
	super Event

	# Create an array of all rooms on the left of the random selected room
	fun list_rooms_left(r: Room, res: Array[Room])
	do
		var left = r.lots[0].left.room
		if left != null then
			res.add left
			list_rooms_left(left, res)
		end
	end

	# Create an array of all rooms on the right of the random selected room
	fun list_rooms_right(r: Room, res: Array[Room])
	do
		var right = r.lots[r.lots.length -1].right.room
		if right != null then
			res.add right
			list_rooms_right(right, res)
		end
	end

	# Suppress rooms that are closed by airlock and static
	fun suppress_closed_rooms(current_to_destroy: Array[Room]): Array[Room]
	do
		var closed = new Array[Int]

		for room in current_to_destroy do
			if room.rule.static or room.rule.airtight then
				closed.add current_to_destroy.index_of(room)
			end
		end

		if closed.not_empty then
			var first_occur = closed[0]
			var last_occur = closed[closed.length - 1]

			for i in [last_occur..first_occur].step(-1) do
				current_to_destroy.remove current_to_destroy[i]
			end
		end

		return current_to_destroy
	end

	# Select a side to destroy if more than one side of the row is not protected by airlock
	fun select_a_side(rooms_to_destroy: Array[Room]): Array[Room]
	do
		var first_half = new Array[Room]
		var second_half = rooms_to_destroy.to_a

		for r in rooms_to_destroy do
			if not rooms_to_destroy.index_of(r) == rooms_to_destroy.length -1 then
				var next_room = rooms_to_destroy[rooms_to_destroy.index_of(r) + 1]

				if not r.lots.last.right.room == next_room then
					for i in [rooms_to_destroy.index_of(next_room)..rooms_to_destroy.length - 1] do
						first_half.add rooms_to_destroy[i]
						second_half.remove rooms_to_destroy[i]
					end
					return [first_half, second_half].rand
				end
			end
		end
		return rooms_to_destroy
	end

	fun find_rooms_to_destroy: Array[Room] do
		var random_room = null

		# Verify if it's a destroyable room
		if not station.rooms.is_empty then
			var potential_room = new Array[Room]
			for room in station.rooms do
				if not room.rule.static and not room.rule.airtight and
				not room.rule == room.rule.story.empty_room then potential_room.add room
			end

			# Select a random room
			if not potential_room.is_empty then random_room = potential_room.rand
		end

		# Get all rooms related to the selected random_room
		var rooms_to_destroy = new Array[Room]
		if not random_room == null then

			# If `random_room` is within the range of a laser defense, nop
			var defenses = [for r in station.rooms do if r isa DefenseRoom then r]
			for defense in defenses do
				var dx = (random_room.lots.first.x - defense.lots.first.x).to_f
				var dy = (random_room.lots.first.y - defense.lots.first.y).to_f
				var r = defense.rule.defense_radius

				if r*r > dx*dx + dy*dy then
					# In range of a defense
					defense.open_fire(10, 10)
					return rooms_to_destroy
				end
			end

			var random_room_left = new Array[Room]
			var random_room_right= new Array[Room]
			list_rooms_left(random_room, random_room_left)
			list_rooms_right(random_room, random_room_right)

			rooms_to_destroy.add_all random_room_left.reversed
			rooms_to_destroy.add random_room
			rooms_to_destroy.add_all random_room_right

			rooms_to_destroy = suppress_closed_rooms(rooms_to_destroy)
			rooms_to_destroy = select_a_side(rooms_to_destroy.to_a)
		end

		return rooms_to_destroy
	end

	redef fun update(turn)
	do
		for room in find_rooms_to_destroy do room.destroy

		station.event = null
	end
end
