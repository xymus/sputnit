module base is serialize

import serialization

abstract class Rule

	var story: Story

	var name: Text = class_name is optional

	redef fun to_s do return name.to_s

	init do story.rules.add self
end

class Story
	var rules = new Array[Rule]
end

abstract class Ruled
	type RULE: Rule
	var rule: RULE
end

abstract class Turnable
	# Apply game logic for `turn`
	fun update(turn: Turn) do end
end

class Turn

	# Time since the launch of the game
	var time: Float

	# Time delta since last turn
	var dt: Float

	# Is the turn the first one of the day (for income and upkeep)
	var new_day = false

	var game: Game
end

class Game

	var station = new Station

	var story = new Story

	# Total game time since the beginning of this game
	var time = 0.0

	# Total length of a single day (in seconds)
	var day_length = 120.0

	# Time of the day within `[0.0..1.0[` where `0.5` is noon
	fun time_of_day: Float
	do
		var day_p = time / day_length
		return day_p - day_p.floor
	end

	# Day count (the integer part), and `time_of_day` (the fractional part)
	fun day: Float do return time / day_length

	# Time of the beginning of the next day
	var next_day: Float = day_length

	# Entrypoint to run a single turn after a wait of `dt` seconds
	#
	# Each modules should refine this method to call `update`
	# on local instances.
	fun update(dt: Float): Turn
	do
		time += dt

		var turn = new Turn(time, dt, self)
		if turn.time > next_day then
			turn.new_day = true
			next_day += day_length
		end
		station.update turn
		return turn
	end

	# Setup a starting world
	fun setup do end
end

class Station
	super Turnable
end
