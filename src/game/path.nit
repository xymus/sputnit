module path is serialize

import room
private import ai::search

# A path solution from a path finding problem
class PathPlan
	var plan: Sequence[Lot]
end

redef class Lot
	# Find a path from self to a given destination
	fun path_to(destination: Lot): nullable PathPlan
	do
		var pp = new PathProblem(self, destination)
		var sol = pp.astar.run
		if sol == null then return null
		var res = new PathPlan(sol.plan)
		return res
	end
end

class PathProblem
	super SearchProblem[Lot, Lot]

	redef var initial_state
	var goal: Lot
	redef fun is_goal(s) do return s == goal

	redef fun actions(s) do return s.walk
	redef fun apply_action(s, a) do return a
	redef fun heuristic(s) do return (s.x.distance(goal.x) + s.y.distance(goal.y)).to_f * 1.5

	redef fun cost(from_lot, action, to_lot)
	do
		var room = to_lot.room
		if room == null then return 1.0

		var rule = room.rule
		if rule isa Stair then return 1.0/rule.vspeed

		return 1.0/rule.hspeed
	end
end

redef class Room
	# Is this room reachable from the terminal
	fun reachable_from_terminal: Bool
	do return lots.first.path_to(station.lot(0, 0)) != null
end

redef class RoomAttempt
	var reachable_from_terminal: Bool is lazy do
		return rule.reachable_from_terminal(self)
	end
end

redef class RoomRule
	# I do not like this method since is duplicates the rules of `connect_walk`
	# TODO find a better approach
	fun reachable_from_terminal(room: RoomAttempt): Bool
	do
		var zero = room.station.lot(0, 0)
		var lot = room.anchor
		if lot.left.path_to(zero) != null then return true
		if lot.move(width, 0).path_to(zero) != null then return true
		return false
	end
end

redef class Stair
	redef fun reachable_from_terminal(room)
	do
		if super then return true
		var zero = room.station.lot(0, 0)
		var lot = room.anchor
		var x = lot.up
		if x.rule == self and x.path_to(zero) != null then return true
		x = lot.down
		if x.rule == self and x.path_to(zero) != null then return true
		return false
	end
end
