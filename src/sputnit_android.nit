import android
intrude import android::audio
import android::vibration

import sputnit

redef class UICamera
	redef fun top_left
	do
		var s = super
		return new Point3d[Float](s.x, s.y - 64.0, s.z)
	end

	redef fun top_right
	do
		var s = super
		return new Point3d[Float](s.x, s.y - 64.0, s.z)
	end
end

redef class App

	redef fun on_create
	do
		super

		# Load sounds manually to skip Nit bug #1728
		for sound in sounds do sound.load

		# Game speed controls
		var scale = 2.0
		var but = new SpeedButton(font.char('-').as(not null),
			ui_camera.bottom_right.offset(-48.0, 64.0, 0.0), 0.5)
		but.scale = scale
		ui_sprites.add but
		buttons.add but

		but = new SpeedButton(font.char('+').as(not null),
			ui_camera.bottom_right.offset(-48.0, 64.0+92.0, 0.0), 2.0)
		but.scale = scale
		ui_sprites.add but
		buttons.add but

		# Lower describe sprites
		sprites_describe.anchor.y -= 160.0
	end
end

redef class EulerCamera
	redef fun accept_scroll_and_zoom(event)
	do
		var r = super
		if r then
			# Clear ghost rooms
			for s in app.over_sprites do app.sprites.remove s
			app.over_sprites.clear
		end
		return r
	end
end

class SpeedButton
	super Button

	var mod: Float

	redef fun act do app.mod_game_speed mod
end

redef class SoundAndVibration
	redef fun play
	do
		app.vibrator.vibrate vibration_ms
		super
	end
end
