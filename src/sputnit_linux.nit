import sputnit
import linux
import json

redef class GamnitDisplay

	init
	do
		width = app.config.res_x
		height = app.config.res_y
	end
end

redef class App

	private var config_path: String = sys.program_name.dirname / "../client_config.json"

	redef var config do
		if config_path.file_exists then
			var content = config_path.to_path.read_all
			var deser = new ConfigDeserializer(content)
			var cc = deser.deserialize

			if cc == null then
				print_error "Error: Deserializing config file failed with {deser.errors.join(", ")}"
			else if not cc isa ClientConfig then
				print_error "Error: Deserializing config file failed, got '{cc}'"
			else return cc
		end

		# Save the default config to pretty Json
		var cc = new ClientConfig
		var json = cc.serialize_to_json(plain=true, pretty=true)
		json.write_to_file config_path

		return cc
	end
end

redef class ClientConfig
	serialize

	# Resolution width
	var res_x = 1280 is lazy

	# Resolution height
	var res_y = 720 is lazy
end

redef class Sound
	redef fun play do if app.config.play_sounds then super
end

# `ClientConfig` deserializer
class ConfigDeserializer
	super JsonDeserializer

	# The only class we deserialize from pretty Json is ClientConfig
	redef fun class_name_heuristic(object) do return "ClientConfig"
end
