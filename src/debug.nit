# Print information on the screen for each callback
module debug

import game

redef class Room
	redef fun on_spawn do
		print "Spawned room {self}"
		super
	end

	redef fun on_destroy do
		print "Destroyed room {self}"
		super
	end
end
